import Footer from "./layout/Footer/Footer";
import Header from "./layout/Header/Header";

export default function BaseLayout({ children }) {
  return (
    <>
      <Header />
      <div className="contentMain">{children}</div>
      <Footer />
    </>
  );
}
