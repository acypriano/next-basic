import styles from './footer.module.css';

export default function Footer () {
    return(
        <>
            <footer className={styles.footer}>
                <p>Projeto desenvolvido em NextJS</p>
            </footer>
        </>
    )
}