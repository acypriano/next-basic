import Link from "next/link";
import NavBar from "../../navigation/NavBar";
import styles from './header.module.css'
export default function Header() {
  return (
    <>
      <header className={styles.header}>
        <Link href="/">
          <a>
            <h1>Logo</h1>
          </a>
        </Link>
        <NavBar />
      </header>
    </>
  );
}
