import  Link  from "next/link";
import styles from "./navbar.module.css";

export default function NavBar() {
  return (
    <>
      <ul className={styles.mainMenu}>
        <li>
          <Link href="/about">
            <a>about</a>
          </Link>
        </li>
        <li>
          <Link href="/job">
            <a>job</a>
          </Link>
        </li>
        <li>
          <Link href="/contact">
            <a>contact</a>
          </Link>
        </li>
        <li>
          <Link href="/teste">
            <a>teste</a>
          </Link>
        </li>
      </ul>
    </>
  );
}
